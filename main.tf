provider "aws"{
    region = "eu-west-3"
    
}

variable vpc_cidr_block{}
variable subnet_cidr_block{}
variable env_prefix{}
variable ab_zone{}
variable myip{}
variable instance_type{}

resource "aws_vpc" "myapp-vpc" {
    cidr_block=var.vpc_cidr_block
    
    tags ={
        Name:"${var.env_prefix}-vpc"
    }
  
}

resource "aws_subnet" "myapp-subnet-1"{
    vpc_id=aws_vpc.myapp-vpc.id
    cidr_block=var.subnet_cidr_block
    availability_zone=var.ab_zone
    tags ={
        Name:"${var.env_prefix}-subnet-1"
    }
}

resource "aws_route_table" "myapp_routetable"{
    vpc_id=aws_vpc.myapp-vpc.id
    route{
        cidr_block="0.0.0.0/0"
        gateway_id=aws_internet_gateway.myapp_internetgateway.id
    }
    tags={
        Name:"${var.env_prefix}-rtb"
    }
}

resource "aws_internet_gateway" "myapp_internetgateway" {
  vpc_id=aws_vpc.myapp-vpc.id
  tags={
        Name:"${var.env_prefix}-igw"
    }
}

resource "aws_route_table_association" "myapp_association"{
    subnet_id=aws_subnet.myapp-subnet-1.id
    route_table_id=aws_route_table.myapp_routetable.id
}


resource "aws_security_group" "myapp-secure"{
    
    name="hero"
    vpc_id=aws_vpc.myapp-vpc.id
    ingress{
        from_port=22
        to_port=22
        protocol="tcp"
        cidr_blocks=[var.myip]
    }

    ingress{
        from_port=8080
        to_port=8080
        protocol="tcp"
        cidr_blocks=["0.0.0.0/0"]

    }
    egress{
        from_port=0
        to_port=0
        protocol="-1"
        cidr_blocks=["0.0.0.0/0"]
        prefix_list_ids=[]

    }

    tags={
        Name:"${var.env_prefix}-sg"
    }
    

}

data "aws_ami" "latest_ami_id"{
    most_recent=true
    owners=["amazon"]
    filter{
        name="name"
        values=["amzn2-ami-hvm-*-x86_64-gp2"]
    }
    filter{
        name="virtualization-type"
        values=["hvm"]
    }
}

resource "aws_instance" "MyEc2"{
    ami=data.aws_ami.latest_ami_id.id
    instance_type=var.instance_type

    subnet_id=aws_subnet.myapp-subnet-1.id
    
    availability_zone=var.ab_zone
    associate_public_ip_address=true
    key_name="Private_File"
    user_data=<<-EOF
              #!/bin/bash
              yum update -y
              amazon-linux-extras install -y lamp-mariadb10.2-php7.2 php7.2
              yum install -y httpd mariadb-server
              systemctl start httpd
              systemctl enable httpd
              usermod -a -G apache ec2-user
              chown -R ec2-user:apache /var/www
              chmod 2775 /var/www
              find /var/www -type d -exec chmod 2775 {} \;
              find /var/www -type f -exec chmod 0664 {} \;
              echo "<?php phpinfo(); ?>" > /var/www/html/phpinfo.php"
              EOF
              

    tags={
        Name:"Karthi's Ec2"
    } 
}